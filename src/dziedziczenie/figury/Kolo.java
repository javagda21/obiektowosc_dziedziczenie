package dziedziczenie.figury;

public class Kolo {
    protected final double PI = 3.14;
    protected double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    public final double obliczPole() {
        return (PI * promien * promien);
    }
}
