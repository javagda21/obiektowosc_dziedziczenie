package dziedziczenie.autka;

public class Kabriolet extends Samochod {
    private boolean dachSchowany;

    public Kabriolet(String kolor, String marka, int rocznik, boolean dachSchowany) {
        super(kolor, marka, rocznik);
        this.dachSchowany = dachSchowany;
    }

    @Override
    public void przyspiesz() {
        if (predkosc <= 170) {
            predkosc += 10;
        } else {
            predkosc = 180;
        }
        System.out.println("Przyspieszam do " + predkosc + " km/h");
    }

    public boolean czyDachSchowany() {
        return dachSchowany;
    }

    public void schowajDach() {
        dachSchowany = true;
    }

    @Override
    public String toString() {
        return super.toString() + " z rozsuwanym dachem";
    }
}
