package dziedziczenie.autka;

public class Main {
    public static void main(String[] args) {
        Kabriolet kabriolet = new Kabriolet("Czerwony", "Marka", 1990, true);

        Samochod samochod = new Samochod("Czerwony", "Maluszek", 1980);

        kabriolet.schowajDach();
//        samochod.schowajDach(); // < nie jest dostępne - samochód nie ma takiej metody

        for (int i = 0; i < 20; i++) {
            kabriolet.przyspiesz();
            samochod.przyspiesz();
        }


    }
}
