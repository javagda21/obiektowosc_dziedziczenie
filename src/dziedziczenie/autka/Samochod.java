package dziedziczenie.autka;

public class Samochod {
    protected int predkosc;
    protected boolean swiatlaWlaczone;

    protected String kolor;
    protected String marka;
    protected int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz() {
        if (predkosc <= 110) {
            predkosc += 10;
        } else {
            predkosc = 120;
        }
        System.out.println("Przyspieszam do " + predkosc + " km/h");
    }

    public boolean czySwiatlaWlaczone() {
        return swiatlaWlaczone;
    }

    public void wlaczSwiatla() {
        swiatlaWlaczone = true;
    }

    @Override
    public String toString() {
        return kolor + " samochód marki " + marka + " rocznik " + rocznik;
    }
}
