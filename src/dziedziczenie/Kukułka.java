package dziedziczenie;

import dziedziczenie.ptaszki.Ptak;

public class Kukułka extends Ptak {

    public Kukułka(String imie, int waga) {
        super(imie, waga);
    }

    public void wypiszCosOKukulce(){
        System.out.println(imie);
    }

    @Override
    public void spiewaj() {
        System.out.println("Ku ku");
    }
}
