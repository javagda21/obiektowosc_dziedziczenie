package dziedziczenie.komputery;

public class Main2 {
    public static void main(String[] args) {
        Laptop k = new Laptop(1, "a", TypProcesora.WIELORDZENIOWY, 1, true);
        k.zrobCos();

        Komputer l = new Laptop(1, "a", TypProcesora.WIELORDZENIOWY, 1, true);
//        Object l = new Object();
        if( l instanceof Laptop) {
            Laptop laptopik = (Laptop) l;
            laptopik.zrobCos();
        }
//        l.zrobCos();

    }
}
