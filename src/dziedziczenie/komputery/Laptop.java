package dziedziczenie.komputery;

public class Laptop extends Komputer{
    private int wielkoscMatrycy;
    private boolean posiadaRetine;

    public Laptop(int potrzebnaMoc,
                  String producent,
                  TypProcesora typProcesora,
                  int wielkoscMatrycy,
                  boolean posiadaRetine) {

        super(potrzebnaMoc, producent, typProcesora);
        this.wielkoscMatrycy = wielkoscMatrycy;
        this.posiadaRetine = posiadaRetine;
    }

    public void zrobCos(){
        System.out.println("Coś");
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "wielkoscMatrycy=" + wielkoscMatrycy +
                ", posiadaRetine=" + posiadaRetine +
                ", potrzebnaMoc=" + potrzebnaMoc +
                ", producent='" + producent + '\'' +
                ", typProcesora=" + typProcesora +
                '}';
    }
}
