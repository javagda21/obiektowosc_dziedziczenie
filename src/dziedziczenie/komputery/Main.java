package dziedziczenie.komputery;

public class Main {
    public static void main(String[] args) {
        Komputer[] komputery = new Komputer[3];
        komputery[0] = new Komputer(100, "Asus", TypProcesora.JEDNORDZENIOWY);
        komputery[1] = new Komputer(100, "Apple", TypProcesora.JEDNORDZENIOWY);
//        komputery[2] = new Komputer(100, "Dell", TypProcesora.WIELORDZENIOWY);
        komputery[2] = new Laptop(10, "a", TypProcesora.WIELORDZENIOWY, 10, true);

        int iterator = 0;
        while (iterator < komputery.length) {
            System.out.println(komputery[iterator]);

            iterator++;
        }

        Laptop[] laptopy = new Laptop[2];
        laptopy[0] = new Laptop(100, "Lap", TypProcesora.WIELORDZENIOWY, 14, true);
        laptopy[1] = new Laptop(100, "Lap", TypProcesora.JEDNORDZENIOWY, 14, false);

        iterator = 0;
        while (iterator < laptopy.length) {
            System.out.println(laptopy[iterator]);

            iterator++;
        }
    }
}
