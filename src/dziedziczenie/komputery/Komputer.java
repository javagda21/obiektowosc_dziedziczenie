package dziedziczenie.komputery;

public class Komputer {
    protected int potrzebnaMoc;
    protected String producent;
    protected TypProcesora typProcesora;

    public Komputer(int potrzebnaMoc, String producent, TypProcesora typProcesora) {
        this.potrzebnaMoc = potrzebnaMoc;
        this.producent = producent;
        this.typProcesora = typProcesora;
    }
}
