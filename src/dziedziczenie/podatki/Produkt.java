package dziedziczenie.podatki;

public class Produkt {
    private String nazwa;
    private double cenaNetto;
    private PodatekProduktu podatekProduktu;

    public Produkt(String nazwa, double cenaNetto, PodatekProduktu podatekProduktu) {
        this.nazwa = nazwa;
        this.cenaNetto = cenaNetto;
        this.podatekProduktu = podatekProduktu;
    }

    public double podajCeneBrutto() {
        return cenaNetto * (podatekProduktu.getPodatek() + 1.0);
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getCenaNetto() {
        return cenaNetto;
    }

    public void setCenaNetto(double cenaNetto) {
        this.cenaNetto = cenaNetto;
    }

    public PodatekProduktu getPodatekProduktu() {
        return podatekProduktu;
    }

    public void setPodatekProduktu(PodatekProduktu podatekProduktu) {
        this.podatekProduktu = podatekProduktu;
    }

    @Override
    public String toString() {
        return "Produkt{" +
                "nazwa='" + nazwa + '\'' +
                ", cenaNetto=" + cenaNetto +
                ", podatekProduktu=" + podatekProduktu +
                '}';
    }
}
