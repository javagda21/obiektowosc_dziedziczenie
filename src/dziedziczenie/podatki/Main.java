package dziedziczenie.podatki;

public class Main {
    public static void main(String[] args) {
        Produkt[] tablica = new Produkt[4];
        tablica[0] = new Produkt("a", 100, PodatekProduktu.NO_VAT);
        tablica[1] = new Produkt("b", 100, PodatekProduktu.VAT_5);
        tablica[2] = new Produkt("c", 100, PodatekProduktu.VAT_8);
        tablica[3] = new Produkt("d", 100, PodatekProduktu.VAT_23);

        for (Produkt produkt : tablica) {
            System.out.println(produkt);
            System.out.println(produkt.podajCeneBrutto());
        }

    }
}
