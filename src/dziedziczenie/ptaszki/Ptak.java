package dziedziczenie.ptaszki;

public class Ptak {
    protected String imie;
    protected int waga;

//    public Ptak() {
//    }

    public Ptak(String imie, int waga) {
        this.imie = imie;
        this.waga = waga;
    }

    public void spiewaj() {
        System.out.println("Ćwir Ćwir");
    }

    @Override
    public String toString() {
        return "Ptak{" +
                "imie=" + imie +
                ", waga=" + waga +
                '}';
    }
}
