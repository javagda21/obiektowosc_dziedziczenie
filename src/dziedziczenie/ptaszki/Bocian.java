package dziedziczenie.ptaszki;

public class Bocian extends Ptak {

    private int ileDziedziPorywaRocznie;

    public Bocian(String imie, int waga, int ileDziedziPorywaRocznie) {
        super(imie, waga);
        this.ileDziedziPorywaRocznie = ileDziedziPorywaRocznie;
    }

    public Bocian(String imie, int waga) {
        super(imie, waga);
    }
//    public Bocian() {
//        // wywołanie konstruktora nadrzędnego
//        super();
//    }

    public void wypiszCosOBocku() {
        System.out.println(imie);
    }

    @Override
    public void spiewaj() {
        System.out.println("Kle kle");
//        super.spiewaj();
    }
}
