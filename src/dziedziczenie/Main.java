package dziedziczenie;

import dziedziczenie.ptaszki.Bocian;
import dziedziczenie.ptaszki.Ptak;

public class Main {
    public static void main(String[] args) {
        Ptak p = new Ptak("Wojtek", 60);
//        p.imie = "Wojtek";
//        p.waga = 60;


        p.spiewaj();

        System.out.println(p);

        Bocian bocian = new Bocian("Bogdan", 70);
        bocian.spiewaj();
        System.out.println(bocian);
    }
}
